"""
=================================
Optimized Naive Bayes spam filter
=================================

Author: Haotian Shi, 2016

"""


import os
import email
import math
from math import log
from heapq import *



############################################################
# Optimized Spam Filter
############################################################

def load_tokens(email_path):
    #email_path = os.getcwd() + '\\' + email_path #'/' + path #"homework5_data\\train\ham\ham1"
    #print path
    with open(email_path) as f:
        message = email.message_from_file(f)
    lines = email.iterators.body_line_iterator(message)
    tokens = []
    for eachLine in lines:#filter ' ' and '\n'
        subtemps = [sub for sub in eachLine.strip().split(' ') if sub != '']
        #for each in subtemps:
            #tokens += [sub for sub in each.split('\n') if sub != '']
        tokens += subtemps
    return tokens

def log_probs(email_paths):
    count = dict()
    for path in email_paths:#["homework5_data\\train\ham\ham%d" % i for i in range(1, 11)]
        f = load_tokens(path)
        for word in f:
            if word not in count:
                count[word] = 1
            else:
                count[word] += 1
    a = 1e-7
    count_sum = sum(count.values())
    V = len(count)
    probs = dict()
    for word in count:
        probs[word] = log((count[word] + a) / (count_sum + a * (V + 1)))
    probs['<UNK>'] = log(a / (count_sum + a * (V + 1)))
    return probs

def bi_log_probs(email_paths):
    bigrams = dict()
    for path in email_paths:#["homework5_data\\train\ham\ham%d" % i for i in range(1, 11)]
        f = load_tokens(path)
        temp = None
        for word in f:
            if temp == None:
                temp = word
            else:
                bi = temp + ' ' + word
                if bi not in bigrams:
                    bigrams[bi] = 1
                else:
                    bigrams[bi] += 1
                temp = word
    a = 1e-15#7
    bi_sum = sum(bigrams.values())
    V_bi = len(bigrams)
    bi_probs = dict()
    for bi in bigrams:
        bi_probs[bi] = log((bigrams[bi] + a) / (bi_sum + a * (V_bi + 1)))
    bi_probs['<UNK>'] = log(a / (bi_sum + a * (V_bi + 1)))
    return bi_probs

def len_log_probs(email_paths):
    lengths = dict()
    for path in email_paths:#["homework5_data\\train\ham\ham%d" % i for i in range(1, 11)]
        f = load_tokens(path)
        for word in f:
            length = len(word)
            if length not in lengths:
                lengths[length] = 1
            else:
                lengths[length] += 1
    a = 1e-7
    len_sum = sum(lengths.values())
    V_len = len(lengths)
    len_probs = dict()
    for length in lengths:
        len_probs[length] = log((lengths[length] + a) / (len_sum + a * (V_len + 1)))
    len_probs['<UNK>'] = log(a / (len_sum + a * (V_len + 1)))
    return len_probs

class SpamFilter(object):

    def __init__(self, spam_dir, ham_dir):
        l_spam = [spam_dir + '/' + filename for filename in os.listdir(spam_dir)]#[spam_dir + '\\' + filename for filename in os.listdir(spam_dir)]
        l_ham = [ham_dir + '/' + filename for filename in os.listdir(ham_dir)]#[ham_dir + '\\' + filename for filename in os.listdir(ham_dir)]
        self.spam_log_probs = log_probs(l_spam)
        self.ham_log_probs = log_probs(l_ham)
        self.spam_bi_log_probs = bi_log_probs(l_spam)
        self.ham_bi_log_probs = bi_log_probs(l_ham)
        self.spam_len_log_probs = len_log_probs(l_spam)
        self.ham_len_log_probs = len_log_probs(l_ham)
        self.prob_spam = float(len(l_spam)) / (len(l_spam) + len(l_ham))
        self.prob_not_spam = 1 - self.prob_spam
    
    def is_spam(self, email_path):
        f = load_tokens(email_path)
        #probability of spam
        count_spam = dict()
        for word in f:
            if word in self.spam_log_probs:
                if word in count_spam:
                    count_spam[word] += 1
                else:
                    count_spam[word] = 1
            else:
                if '<UNK>' in count_spam:
                    count_spam['<UNK>'] += 1
                else:
                    count_spam['<UNK>'] = 1
        p_spam = 0
        for word in count_spam:
            p_spam += self.spam_log_probs[word] * count_spam[word]
        p_spam += log(self.prob_spam)
        #probability of ham
        count_ham = dict()
        for word in f:
            if word in self.ham_log_probs:
                if word in count_ham:
                    count_ham[word] += 1
                else:
                    count_ham[word] = 1
            else:
                if '<UNK>' in count_ham:
                    count_ham['<UNK>'] += 1
                else:
                    count_ham['<UNK>'] = 1
        p_ham = 0
        for word in count_ham:
            p_ham += self.ham_log_probs[word] * count_ham[word]
        p_ham += log(self.prob_not_spam)
        #probability of spam by bigram
        bi_count_spam = dict()
        temp = None
        for word in f:
            if temp == None:
                temp = word
            else:
                bi = temp + ' ' + word
                if bi in self.spam_bi_log_probs:
                    if bi in bi_count_spam:
                        bi_count_spam[bi] += 1
                    else:
                        bi_count_spam[bi] = 1
                else:
                    if '<UNK>' in bi_count_spam:
                        bi_count_spam['<UNK>'] += 1
                    else:
                        bi_count_spam['<UNK>'] = 1
                temp = word
        bi_p_spam = 0
        for bi in bi_count_spam:
            bi_p_spam += self.spam_bi_log_probs[bi] * bi_count_spam[bi]
        bi_p_spam += log(self.prob_spam)
        #probability of ham by bigram
        bi_count_ham = dict()
        temp = None
        for word in f:
            if temp == None:
                temp = word
            else:
                bi = temp + ' ' + word
                if bi in self.ham_bi_log_probs:
                    if bi in bi_count_ham:
                        bi_count_ham[bi] += 1
                    else:
                        bi_count_ham[bi] = 1
                else:
                    if '<UNK>' in bi_count_ham:
                        bi_count_ham['<UNK>'] += 1
                    else:
                        bi_count_ham['<UNK>'] = 1
                temp = word
        bi_p_ham = 0
        for bi in bi_count_ham:
            bi_p_ham += self.ham_bi_log_probs[bi] * bi_count_ham[bi]
        bi_p_ham += log(self.prob_not_spam)
        #probability of spam by length
        len_count_spam = dict()
        for word in f:
            length = len(word)
            if length in self.spam_len_log_probs:
                if length in len_count_spam:
                    len_count_spam[length] += 1
                else:
                    len_count_spam[length] = 1
            else:
                if '<UNK>' in len_count_spam:
                    len_count_spam['<UNK>'] += 1
                else:
                    len_count_spam['<UNK>'] = 1
        len_p_spam = 0
        for length in len_count_spam:
            len_p_spam += self.spam_len_log_probs[length] * len_count_spam[length]
        len_p_spam += log(self.prob_spam)
        #probability of ham by length
        len_count_ham = dict()
        for word in f:
            length = len(word)
            if length in self.ham_len_log_probs:
                if length in len_count_ham:
                    len_count_ham[length] += 1
                else:
                    len_count_ham[length] = 1
            else:
                if '<UNK>' in len_count_ham:
                    len_count_ham['<UNK>'] += 1
                else:
                    len_count_ham['<UNK>'] = 1
        len_p_ham = 0
        for length in len_count_ham:
            len_p_ham += self.ham_len_log_probs[length] * len_count_ham[length]
        len_p_ham += log(self.prob_spam)
        #filter
        if p_spam + bi_p_spam + len_p_spam > p_ham + bi_p_ham + len_p_ham:
            return True
        return False
        '''
        if p_spam > p_ham or bi_p_spam > bi_p_ham or len_p_spam > len_p_ham:
            return True
        return False
        '''
        '''
        if p_spam < p_ham and bi_p_spam < bi_p_ham:
            return False
        elif p_spam > p_ham or bi_p_spam > bi_p_ham or len_p_spam > len_p_ham:
            return True
        else:
            return False
        '''


if __name__ == '__main__':
    sf = SpamFilter("data\\train\spam", "data\\train\ham")
    spam_dir = "data\\train\spam"
    l_spam = [spam_dir + '\\' + filename for filename in os.listdir(spam_dir)]
    ham_dir = "data\\train\ham"
    l_ham = [ham_dir + '\\' + filename for filename in os.listdir(ham_dir)]
    cspam = 0
    cham = 0
    for spam in l_spam:
        if sf.is_spam(spam):
            cspam += 1
    for ham in l_ham:
        if not sf.is_spam(ham):
            cham += 1
    print 'train spam accuracy:', float(cspam)/len(l_spam)
    print 'train ham accuracy:', float(cham)/len(l_ham)
    spam_dir = "data\dev\spam"
    l_spam = [spam_dir + '\\' + filename for filename in os.listdir(spam_dir)]
    ham_dir = "data\dev\ham"
    l_ham = [ham_dir + '\\' + filename for filename in os.listdir(ham_dir)]
    cspam = 0
    cham = 0
    for spam in l_spam:
        if sf.is_spam(spam):
            cspam += 1
    for ham in l_ham:
        if not sf.is_spam(ham):
            cham += 1
    print 'dev spam accuracy:', float(cspam)/len(l_spam)
    print 'dev ham accuracy:', float(cham)/len(l_ham)

