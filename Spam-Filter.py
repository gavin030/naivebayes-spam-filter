"""
=======================
Naive Bayes spam filter
=======================

Author: Haotian Shi, 2016

"""


import os
import email
import math
from math import log
from heapq import *



############################################################
# Spam Filter
############################################################

def load_tokens(email_path):
    #email_path = os.getcwd() + '\\' + email_path #'/' + path #"homework5_data\\train\ham\ham1"
    #print path
    with open(email_path) as f:
        message = email.message_from_file(f)
    lines = email.iterators.body_line_iterator(message)
    tokens = []
    for eachLine in lines:#filter ' ' and '\n'
        subtemps = [sub for sub in eachLine.replace('\t', ' ').strip().split(' ') if sub != '']
        #for each in subtemps:
            #tokens += [sub for sub in each.split('\n') if sub != '']
        tokens += subtemps
    return tokens

def log_probs(email_paths, smoothing):
    count = dict()
    for path in email_paths:#["homework5_data\\train\ham\ham%d" % i for i in range(1, 11)]
        f = load_tokens(path)
        for word in f:
            if word not in count:
                count[word] = 1
            else:
                count[word] += 1
    count_sum = sum(count.values())
    V = len(count)
    a = smoothing
    probs = dict()
    for word in count:
        probs[word] = log((count[word] + a) / (count_sum + a * (V + 1)))
    probs['<UNK>'] = log(a / (count_sum + a * (V + 1)))
    return probs

class SpamFilter(object):

    def __init__(self, spam_dir, ham_dir, smoothing):
        l_spam = [spam_dir + '/' + filename for filename in os.listdir(spam_dir)]#[spam_dir + '\\' + filename for filename in os.listdir(spam_dir)]
        l_ham = [ham_dir + '/' + filename for filename in os.listdir(ham_dir)]#[ham_dir + '\\' + filename for filename in os.listdir(ham_dir)]
        self.spam_log_probs = log_probs(l_spam, smoothing)
        self.ham_log_probs = log_probs(l_ham, smoothing)
        self.prob_spam = float(len(l_spam)) / (len(l_spam) + len(l_ham))
        self.prob_not_spam = 1 - self.prob_spam
    
    def is_spam(self, email_path):
        f = load_tokens(email_path)
        count_spam = dict()
        for word in f:
            if word in self.spam_log_probs:
                if word in count_spam:
                    count_spam[word] += 1
                else:
                    count_spam[word] = 1
            else:
                if '<UNK>' in count_spam:
                    count_spam['<UNK>'] += 1
                else:
                    count_spam['<UNK>'] = 1
        p_spam = 0
        for word in count_spam:
            p_spam += self.spam_log_probs[word] * count_spam[word]
        p_spam += log(self.prob_spam)
        count_ham = dict()
        for word in f:
            if word in self.ham_log_probs:
                if word in count_ham:
                    count_ham[word] += 1
                else:
                    count_ham[word] = 1
            else:
                if '<UNK>' in count_ham:
                    count_ham['<UNK>'] += 1
                else:
                    count_ham['<UNK>'] = 1
        p_ham = 0
        for word in count_ham:
            p_ham += self.ham_log_probs[word] * count_ham[word]
        p_ham += log(self.prob_not_spam)
        if p_spam > p_ham:
            return True
        return False

    def most_indicative_spam(self, n):
        heap = []
        store = dict()
        for word in self.spam_log_probs:
            if word in self.ham_log_probs and word != '<UNK>':
                alter_indicative_value = (log(self.prob_spam) + (self.ham_log_probs[word] - self.spam_log_probs[word]) * log(self.prob_not_spam))
                if len(heap) == n:
                    if alter_indicative_value > heap[0]:
                        cancel = heappop(heap)
                        store[cancel].pop()
                        if len(store[cancel]) == 0:
                            del store[cancel]
                        heappush(heap, alter_indicative_value)
                        if alter_indicative_value in store:
                            store[alter_indicative_value].append(word)
                        else:
                            store[alter_indicative_value] = [word]
                        #print heap
                        #print store
                else:
                    heap.append(alter_indicative_value)
                    if alter_indicative_value in store:
                        store[alter_indicative_value].append(word)
                    else:
                        store[alter_indicative_value] = [word]
                    #print heap
                    #print store
                    if len(heap) == n:
                        heapify(heap)
        words = []
        while(len(heap) > 0):
            words.append(store[heappop(heap)].pop())
        words.reverse()
        return words

    def most_indicative_ham(self, n):
        heap = []
        store = dict()
        for word in self.ham_log_probs:
            if word in self.spam_log_probs and word != '<UNK>':
                alter_indicative_value = (log(self.prob_not_spam) + (self.spam_log_probs[word] - self.ham_log_probs[word]) * log(self.prob_spam))
                if len(heap) == n:
                    if alter_indicative_value > heap[0]:
                        cancel = heappop(heap)
                        store[cancel].pop()
                        if len(store[cancel]) == 0:
                            del store[cancel]
                        heappush(heap, alter_indicative_value)
                        if alter_indicative_value in store:
                            store[alter_indicative_value].append(word)
                        else:
                            store[alter_indicative_value] = [word]
                        #print heap
                        #print store
                else:
                    heap.append(alter_indicative_value)
                    if alter_indicative_value in store:
                        store[alter_indicative_value].append(word)
                    else:
                        store[alter_indicative_value] = [word]
                    #print heap
                    #print store
                    if len(heap) == n:
                        heapify(heap)
        words = []
        while(len(heap) > 0):
            words.append(store[heappop(heap)].pop())
        words.reverse()
        return words

if __name__ == '__main__':
    ham_dir = "data\\train\ham/"
    print load_tokens(ham_dir+"ham1")[200:204]
    print load_tokens(ham_dir+"ham2")[110:114]

    spam_dir = "data\\train\spam/"
    print load_tokens(spam_dir+"spam1")[1:5]
    print load_tokens(spam_dir+"spam2")[:4]

    paths = ["data\\train\ham\ham%d" % i for i in range(1, 11)]
    p = log_probs(paths, 1e-5)
    print p["the"]
    print p["line"]

    paths = ["data\\train\spam\spam%d" % i for i in range(1, 11)]
    p = log_probs(paths, 1e-5)
    print p["Credit"]
    print p["<UNK>"]

    sf = SpamFilter("data\\train\spam", "data\\train\ham", 1e-5)
    print sf.most_indicative_spam(5)
    print sf.most_indicative_ham(5)

    sf = SpamFilter("data\\train\spam", "data\\train\ham", 1e-5)
    spam_dir = "data\\train\spam"
    l_spam = [spam_dir + '\\' + filename for filename in os.listdir(spam_dir)]
    ham_dir = "data\\train\ham"
    l_ham = [ham_dir + '\\' + filename for filename in os.listdir(ham_dir)]
    cspam = 0
    cham = 0
    for spam in l_spam:
        if sf.is_spam(spam):
            cspam += 1
    for ham in l_ham:
        if not sf.is_spam(ham):
            cham += 1
    print 'train spam accuracy:', float(cspam)/len(l_spam)
    print 'train ham accuracy:', float(cham)/len(l_ham)
    spam_dir = "data\dev\spam"
    l_spam = [spam_dir + '\\' + filename for filename in os.listdir(spam_dir)]
    ham_dir = "data\dev\ham"
    l_ham = [ham_dir + '\\' + filename for filename in os.listdir(ham_dir)]
    cspam = 0
    cham = 0
    for spam in l_spam:
        if sf.is_spam(spam):
            cspam += 1
    for ham in l_ham:
        if not sf.is_spam(ham):
            cham += 1
    print 'dev spam accuracy:', float(cspam)/len(l_spam)
    print 'dev ham accuracy:', float(cham)/len(l_ham)

